plugins {
    java
    id("idea")
    id("io.freefair.lombok")
    id("io.spring.dependency-management")
//    id("nebula.integtest")
    id("org.springdoc.openapi-gradle-plugin")
    id("org.springframework.boot")
}

group = "ru.skillbox.socialnetwork"
version = "0.0.1"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

dependencyManagement {
    val testcontainersVersion: String by project
    imports {
        mavenBom("org.testcontainers:testcontainers-bom:$testcontainersVersion")
    }
}

dependencies {
    val lombokMapstructVersion: String by project
    val mapstructVersion: String by project
    val springdocOpenapiVersion: String by project

    implementation("org.apache.commons:commons-lang3:3.13.0")
    implementation("org.liquibase:liquibase-core:4.28.0")

    implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:$springdocOpenapiVersion")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-web")

    runtimeOnly("org.postgresql:postgresql")

    // mapstruct
    implementation("org.mapstruct:mapstruct:$mapstructVersion")
    implementation("org.projectlombok:lombok-mapstruct-binding:$lombokMapstructVersion")
    testImplementation("org.mapstruct:mapstruct:$mapstructVersion")
    testImplementation("org.projectlombok:lombok-mapstruct-binding:$lombokMapstructVersion")
    annotationProcessor("org.mapstruct:mapstruct-processor:$mapstructVersion")
    testAnnotationProcessor("org.mapstruct:mapstruct-processor:$mapstructVersion")
    testImplementation("org.mapstruct:mapstruct-processor:${mapstructVersion}")
    testAnnotationProcessor("org.mapstruct:mapstruct-processor:${mapstructVersion}")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.cloud:spring-cloud-contract-wiremock:4.0.4")
    testImplementation("org.testcontainers:junit-jupiter")
    testImplementation("org.testcontainers:postgresql")
}

springBoot {
    buildInfo()
}

tasks.jar {
    enabled = false
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed", "standardOut", "standardError")
        exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
    }
}

tasks.named<Test>("test") {
    exclude("**/*SmokeTest*","**/*RegressTest*")
}

tasks.register<Test>("Smoke") {
    useJUnitPlatform()
    include("**/*SmokeTest*")
}

tasks.register<Test>("Regress") {
    useJUnitPlatform()
    include("**/*RegressTest*")
}