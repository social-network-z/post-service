pluginManagement {
    val dependencyManagementPluginVersion: String by settings
    val springBootPluginVersion: String by settings
    val lombokPluginVersion: String by settings
    val nebulaIntegtestPluginVersion: String by settings
    val springdocOpenapiPluginVersion: String by settings
    plugins {
        id("io.freefair.lombok") version lombokPluginVersion
        id("io.spring.dependency-management") version dependencyManagementPluginVersion
        id("nebula.integtest") version nebulaIntegtestPluginVersion
        id("org.springdoc.openapi-gradle-plugin") version springdocOpenapiPluginVersion
        id("org.springframework.boot") version springBootPluginVersion
    }
    repositories {
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositories {
        mavenCentral()
    }
}
rootProject.name = "post-service"
