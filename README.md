# social-network

---
### Учебный проект Skillbox

## post-service

Профиль 'dev' использует liquibase для генерации таблиц БД.\
Профиль 'ddl' автоматически генерирует ddl в БД.

---
Open API (Swagger):

http://localhost:8080/swagger-ui.html

http://localhost:8080/v3/api-docs

---
http://localhost:9090/actuator

http://localhost:9090/actuator/swagger-ui

http://localhost:9090/actuator/openapi
