package ru.skillbox.socialnetwork.postservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.skillbox.socialnetwork.postservice.domain.entity.Post;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    @Query(value = "SELECT p from Post p left join fetch p.photos where p.id = :id")
    Optional<Post> findByIdWithPhotos(@Param("id") Long id);

    List<Post> findAllByUserId(Long userId);

}
