package ru.skillbox.socialnetwork.postservice.domain.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "photos", schema = "posts_scheme")
public class Photo extends BaseListener {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false, columnDefinition = "bigserial not null primary key")
  private Long id;

  @NotBlank
  @Size(min = 4)
  @Column(nullable = false, unique = true)
  private String name;

  @Column
  private String description;

  @Column
  private LocalDateTime dateTime;

  @Column
  private String location;

  @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
  @JoinColumn(name = "file_id",
          referencedColumnName = "id",
          foreignKey = @ForeignKey(name = "fk_photos_file_id"))
  private S3File file;

  @ManyToOne()
  @JoinColumn(name = "post_id",
          referencedColumnName = "id",
          foreignKey = @ForeignKey(name = "fk_photos_post_id"))
  private Post post;

}
