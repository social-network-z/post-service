package ru.skillbox.socialnetwork.postservice.domain.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDateTime;

@Data
@Validated
public class PhotoDto {

  private Long id;
  private String name;
  private String description;
  private LocalDateTime dateTime;
  private String location;
  private String url;
  @NotNull
  private Long postId;

}
