package ru.skillbox.socialnetwork.postservice.domain.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.URL;
import ru.skillbox.socialnetwork.postservice.domain.enums.S3FileTypes;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "s3files", schema = "posts_scheme")
public class S3File extends BaseListener {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false, columnDefinition = "bigserial not null primary key")
  private Long id;

  @Enumerated(EnumType.STRING)
  @NotNull
  @Column(name = "type", nullable = false)
  private S3FileTypes type;

  @URL
  @NotBlank
  @Column(nullable = false)
  private String url;

  @NotBlank
  @Column(name = "file_name_in_s3", nullable = false)
  private String fileNameInS3;
}
