package ru.skillbox.socialnetwork.postservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.socialnetwork.postservice.domain.dto.PostDto;
import ru.skillbox.socialnetwork.postservice.mapping.PostMapper;
import ru.skillbox.socialnetwork.postservice.service.PostService;

import java.util.List;

@Tag(name = "PostController")
@RestController
@RequestMapping("posts")
@RequiredArgsConstructor
public class PostController {

    private final PostMapper postMapper;
    private final PostService postService;

    @Operation(description = "Создание нового поста")
    @PostMapping
    public ResponseEntity<PostDto> createPost(@RequestBody PostDto postDto) {

        var post = postService.create(postDto);
        return ResponseEntity.ok().body(postMapper.toDto(post));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deletePost(@PathVariable long id) {

        postService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<PostDto> getPostById(@PathVariable long id) {

        return ResponseEntity.ok(postMapper.toDto(postService.getPostById(id)));
    }

    @GetMapping("/userId/{userId}")
    public ResponseEntity<List<PostDto>> getPostByUserId(@PathVariable @NotBlank long userId) {

        return ResponseEntity.ok(postMapper.toDtoList(postService.getPostByUserId(userId)));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PostDto> updatePost(@PathVariable("id") long id,
                                              @Validated @RequestBody PostDto postDto) {

        return ResponseEntity.ok(postMapper.toDto(postService.update(id, postDto)));
    }

    @GetMapping
    public ResponseEntity<List<PostDto>> getPosts() {

        return ResponseEntity.ok(postMapper.toDtoList(postService.getAllPosts()));
    }
}
