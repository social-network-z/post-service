package ru.skillbox.socialnetwork.postservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.skillbox.socialnetwork.postservice.domain.dto.PhotoDto;
import ru.skillbox.socialnetwork.postservice.mapping.PhotoMapper;
import ru.skillbox.socialnetwork.postservice.service.PhotoService;

@Tag(name = "PhotoController")
@RestController
@RequestMapping("posts/photos")
@RequiredArgsConstructor
@Validated
public class PhotoController {

    private final PhotoService photoService;
    private final PhotoMapper photoMapper;

    @Operation(description = "Добавление фотографии поста")
    @PostMapping
    public ResponseEntity<PhotoDto> createPhoto(@Valid @RequestBody PhotoDto photoDto,
                                                @NotNull @RequestParam MultipartFile file) {

        var photo = photoService.create(photoDto, file);
        var dto = photoMapper.toDto(photo);
        return ResponseEntity.ok().body(dto);
    }

    @Operation(description = "Удаление фотографии поста")
    @DeleteMapping("{photoId}")
    public ResponseEntity<HttpStatus> deletePhoto(@PathVariable long photoId) {

        photoService.delete(photoId);
        return ResponseEntity.ok().build();
    }

    @Operation(description = "Получить ссылку на фото поста")
    @GetMapping("{photoId}")
    public ResponseEntity<PhotoDto> getPhoto(@PathVariable long photoId) {

        var photo = photoService.getPhotoById(photoId);
        var dto = photoMapper.toDto(photo);
        return ResponseEntity.ok().body(dto);
    }

    @Operation(description = "Обновить фото поста")
    @PutMapping
    public ResponseEntity<PhotoDto> updatePhoto(@Valid @RequestBody PhotoDto photoDto,
                                                @RequestParam MultipartFile file) {

        var photo = photoService.update(photoDto, file);
        var dto = photoMapper.toDto(photo);
        return ResponseEntity.ok().body(dto);
    }
}
