package ru.skillbox.socialnetwork.postservice.service;

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.socialnetwork.postservice.domain.dto.PostDto;
import ru.skillbox.socialnetwork.postservice.domain.entity.Post;
import ru.skillbox.socialnetwork.postservice.repository.PostRepository;

import java.util.List;

import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class PostService {

    private final EntityManager entityManager;
    private final PostRepository postRepository;

    public Post create(PostDto postDto) {
        var post = Post.builder()
                .userId(postDto.getUserId())
                .name(postDto.getName())
                .build();
        post = update(post);
        log.info("Create new post id {}, user id {}, name '{}'", post.getId(), post.getUserId(), post.getName());
        return getPostById(post.getId());
    }

    public void delete(long id) {
        checkExist(id);
        postRepository.deleteById(id);
        log.info("Post ID {} has been deleted", id);
    }

    private void checkExist(long id) {
        if (!postRepository.existsById(id)) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "Post ID " + id + " not found");
        }
    }

    public Post getPostById(Long id) {
        return postRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        format("Post ID %s not found", id)));
    }

    public Post getPostWithPhotos(Long id) {
        return postRepository.findByIdWithPhotos(id)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        format("Post ID %s not found", id)));
    }

    public List<Post> getPostByUserId(Long userId) {
        return postRepository.findAllByUserId(userId);
    }

    public Post update(Post post) {
        return postRepository.save(post);
    }

    public Post update(Long id, PostDto postDto) {
        var post = getPostById(id);
        if (StringUtils.isNotEmpty(postDto.getName())) {
            post.setName(postDto.getName());
        }
        return postRepository.save(post);
    }

    // Admin service
    public List<Post> getAllPosts(boolean isDeleted) {
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedPostFilter");
        filter.setParameter("isDeleted", isDeleted);
        List<Post> products = postRepository.findAll();
        session.disableFilter("deletedPostFilter");
        return products;
    }

    public List<Post> getAllPosts() {
        return postRepository.findAll();
    }
}
