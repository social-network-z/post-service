package ru.skillbox.socialnetwork.postservice.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.socialnetwork.postservice.domain.dto.PhotoDto;
import ru.skillbox.socialnetwork.postservice.domain.entity.Photo;
import ru.skillbox.socialnetwork.postservice.repository.PhotoRepository;

import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class PhotoService {

    private final PostService postService;
    private final PhotoRepository photoRepository;

    public Photo create(PhotoDto photoDto, MultipartFile file) {
        var post = postService.getPostById(photoDto.getPostId());
        var photo = Photo.builder()
                .post(post)
                .build();
        update(photo, photoDto);
        update(photo, file);
        // проверить обновление
        photo = photoRepository.save(photo);
        log.debug("Photo id {} has been created", photo.getId());
        return photo;
    }

    public void delete(long photoId) {
        checkExist(photoId);
        photoRepository.deleteById(photoId);
        log.info("Photo id {} has been deleted", photoId);
    }

    private void checkExist(long photoId) {
        if (!photoRepository.existsById(photoId)) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    format("Photo id %s not found", photoId));
        }
    }

    public Photo getPhotoById(Long photoId) {
        return photoRepository.findById(photoId)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        format("Photo id %s not found", photoId)));
    }

    public Photo update(PhotoDto photoDto, MultipartFile file) {
        if (photoDto.getId() == null) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "Photo id cannot be null");
        }
        var photo = getPhotoById(photoDto.getId());
        update(photo, photoDto);
        update(photo, file);
        // проверить обновление
        photo = photoRepository.save(photo);
        log.debug("Photo id {} has been updated", photo.getId());
        return photo;
    }

    private void update(Photo photo, PhotoDto photoDto) {
        if (StringUtils.isNotEmpty(photoDto.getName())) {
            photo.setName(photoDto.getName());
        }
        if (StringUtils.isNotEmpty(photoDto.getDescription())) {
            photo.setDescription(photoDto.getDescription());
        }
        if (StringUtils.isNotEmpty(photoDto.getLocation())) {
            photo.setLocation(photoDto.getLocation());
        }
        if (photoDto.getDateTime() != null) {
            photo.setDateTime(photoDto.getDateTime());
        }
    }

    private void update(Photo photo, MultipartFile file) {
        if (file != null) {
            var s3File = photo.getFile();
            // TODO проверить наличие в S3
            s3File.setFileNameInS3(file.getOriginalFilename());
            photo.setFile(s3File);
        }
    }
}
