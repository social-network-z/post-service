package ru.skillbox.socialnetwork.postservice.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.Collection;

@Slf4j
public class MergeUtils {

    public static <ModelT, DtoT> ModelT updateChangesIfNotNull(ModelT first, DtoT second) {
        try {
            Field[] fields = first.getClass().getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                Object value = field.get(second);
                if (value instanceof Collection) {
                    if (!CollectionUtils.isEmpty((Collection<?>) value)) {
                        field.set(first, value);
                    }
                } else {
                    if (value != null) {
                        field.set(first, value);
                    }
                }
            }
        } catch (IllegalAccessException e) {
            log.error("Fail to merge fields of {} and {}", first, second);
            throw new IllegalArgumentException("Fail to merge fields", e);
        }
        return first;
    }

    public static <ModelT, DtoT> ModelT updateFieldIfNotNull(String fieldName, ModelT first, DtoT second) {
        try {
            var fieldF = first.getClass().getDeclaredField(fieldName);
            var fieldS = second.getClass().getDeclaredField(fieldName);
            fieldF.setAccessible(true);
            fieldS.setAccessible(true);
            var value = fieldS.get(second);
            if (value instanceof Collection) {
                if (!CollectionUtils.isEmpty((Collection<?>) value)) {
                    fieldF.set(first, value);
                }
            } else {
                if (value != null) {
                    fieldF.set(first, value);
                }
            }
        } catch (IllegalAccessException e) {
            log.error("Fail to update field {} of {} and {}", fieldName, first, second);
            throw new IllegalArgumentException("Fail to update field", e);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
        return first;
    }
}
