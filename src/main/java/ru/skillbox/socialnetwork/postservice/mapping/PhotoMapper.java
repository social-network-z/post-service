package ru.skillbox.socialnetwork.postservice.mapping;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.springframework.stereotype.Component;
import ru.skillbox.socialnetwork.postservice.domain.dto.PhotoDto;
import ru.skillbox.socialnetwork.postservice.domain.entity.Photo;

import java.util.List;

@Component
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface PhotoMapper {

    PhotoDto toDto(Photo photo);

    List<PhotoDto> toDtoList(List<Photo> posts);

}
