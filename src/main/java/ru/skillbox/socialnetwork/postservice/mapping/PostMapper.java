package ru.skillbox.socialnetwork.postservice.mapping;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.springframework.stereotype.Component;
import ru.skillbox.socialnetwork.postservice.domain.dto.PostDto;
import ru.skillbox.socialnetwork.postservice.domain.entity.Post;

import java.util.List;

@Component
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface PostMapper {

    PostDto toDto(Post post);

    List<PostDto> toDtoList(List<Post> posts);

}
