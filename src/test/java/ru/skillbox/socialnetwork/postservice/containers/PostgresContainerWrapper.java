package ru.skillbox.socialnetwork.postservice.containers;

import lombok.extern.slf4j.Slf4j;
import org.testcontainers.containers.PostgreSQLContainer;

@Slf4j
public class PostgresContainerWrapper extends PostgreSQLContainer<PostgresContainerWrapper> {
    private static final String POSTGRES_IMAGE_NAME = "postgres:16-alpine3.18";
    private static String POSTGRES_DB = "test_posts";
    private static final String POSTGRES_USER = "test";
    private static final String POSTGRES_PASSWORD = "test";

    public PostgresContainerWrapper() {
        super(POSTGRES_IMAGE_NAME);
        setContainerProperties();
    }

    public PostgresContainerWrapper(String dbName) {
        super(POSTGRES_IMAGE_NAME);
        POSTGRES_DB = dbName;
        setContainerProperties();
    }

    @Override
    public void start() {
        super.start();
        var id = this.getContainerId();
        log.debug("Start postgres test container with id {}",id);
        // debug point. Container has to be already started
    }

    private void setContainerProperties() {
        this
                // if you need container logger
                // .withLogConsumer(new Slf4jLogConsumer(log))
                .withDatabaseName(POSTGRES_DB)
                .withUsername(POSTGRES_USER)
                .withPassword(POSTGRES_PASSWORD)
                .withUrlParam("currentSchema","posts_scheme")
        ;
//                .withCopyFileToContainer(MountableFile.forClasspathResource("source_on_host"),
//                        "destination_in_container");
    }
}
