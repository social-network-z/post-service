package ru.skillbox.socialnetwork.postservice.util;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

public abstract class RestUtils {

    protected static String basePath ="/posts";

    public static void validationRestStub(HttpStatus status, String responsePath) {
        MappingBuilder mappingBuilder = WireMock.post(WireMock.urlEqualTo(basePath));
        ResponseDefinitionBuilder responseDefinitionBuilder = WireMock.aResponse()
                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .withStatus(status.value())
                .withBodyFile(responsePath);
        WireMock.givenThat(mappingBuilder.willReturn(responseDefinitionBuilder));
    }
}
