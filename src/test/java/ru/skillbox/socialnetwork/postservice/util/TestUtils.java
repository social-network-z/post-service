package ru.skillbox.socialnetwork.postservice.util;

import lombok.SneakyThrows;
import wiremock.com.fasterxml.jackson.databind.ObjectMapper;
import wiremock.org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

public abstract class TestUtils {

    protected static final ObjectMapper MAPPER = new ObjectMapper();

    @SneakyThrows
    public static String classpathFileToString(String filePath) {
        return FileUtils.readFileToString(
                new File(TestUtils.class.getResource(filePath).getPath()), StandardCharsets.UTF_8.name()
        );
    }

    @SneakyThrows
    public static <T> T classpathFileToObject(String filePath, Class<T> clazz) {
        return MAPPER.readValue(new FileInputStream(TestUtils.class.getResource(filePath).getPath()), clazz);
    }

    @SneakyThrows
    public static <T> List<T> classpathFileToObjectList(String filePath, Class<T> clazz) {
        return MAPPER.readValue(new FileInputStream(TestUtils.class.getResource(filePath).getPath()),
                MAPPER.getTypeFactory().constructCollectionType(List.class, clazz));
    }

    @SneakyThrows
    public static String objectToString(Object object) {
        return MAPPER.writeValueAsString(object);
    }

    @SneakyThrows
    public static <T> T stringToObject(String str, Class<T> clazz) {
        return MAPPER.readValue(str, clazz);
    }

    @SneakyThrows
    public static <T> List<T> jsonToObject(String json, Class<T> clazz) {
        return MAPPER.readValue(json, MAPPER.getTypeFactory().constructCollectionType(List.class, clazz));
    }
}
